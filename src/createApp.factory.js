import { domFactory } from "./dom.factory.js";
import { html, css } from "./tagged.templates.js";

export const getPropsFrom = (element) => {
	const props = {}

  if (!element) return null;
  if (!element.dataset) return null;

	for (let key in element.dataset) {

		const propValue = element.dataset[key]
		if(/^\w/.test(propValue)) {
			Object.assign(props, {[key]:propValue})
		}

    if(/^\#\/[\w]*$/.test(propValue)) {
      Object.assign(props, {[key]:propValue})
    }

		if(/^\{|\[/.test(propValue)) {
			const data = propValue
				.replace(/\'/g, '"')
				.replace(/(\r\n|\n|\r)/gm,"")
				.replace(/(\[\s*)/g, '[')
				.replace(/(\s*\])/g, ']')
				.replace(/(\},\s*)/g, '},')

			props[key] = JSON.parse(data)
		}
  }
  return props;
};

export const createApp = (factory, context = document.body) => {
  const _dom = domFactory();

  const _createSelector = (text) => {
    return text
      .split(/(?=[A-Z])/)
      .join("-")
      .toLowerCase();
  };

  const _bindListeners = (component) => {
    const events = _getEvents(component);

    for (let key in events) {
      events[key]();
    }
  };

  const _renderChildren = (component) => {
    const children = _getChildren(component);

    const childrenComponents = children.map((child) => {
      return createApp(child, component.element);
    });

    childrenComponents.forEach((child) => child.init());
  };

  const _getEvents = (component) => {
    if (!component.hasOwnProperty("events")) {
      return { events: () => ({}) };
    }

    const methods = _getProp("methods", component);

    const eventParams = {
      on: _dom.on,
      queryOnce: (selector) => _dom.queryOnce(selector, component.element),
      queryAll: (selector) => _dom.queryAll(selector, component.element),
      methods,
    };

    return component.events(eventParams);
  };

  const _getChildren = (component) => {
    const children = _getProp("children", component);
    if (Array.isArray(children)) return children;
    return [];
  };

  const _getProp = (key, component) => {
    if (!component.hasOwnProperty(key)) return;
    if (typeof component[key] !== "function") return;
    return component[key]();
  };

	const _execHooks = (hookName, component) => {
		if (!component.hasOwnProperty("hooks")) return;
		if (typeof component.hooks !== "function") return;
		const methods = _getProp("methods", component);
		const hooks = component.hooks({ methods });
		if (!hooks.hasOwnProperty(hookName)) return;
		hooks[hookName]();
	};

  const _bindStyles = (styles) => {
    const selector = _createSelector(factory.name);
    const styleExists = document.querySelector(`style#${selector}`)
      ? true
      : false;
    if (styleExists) return;

    const styleElement = document.createElement("style");
    styleElement.setAttribute("id", selector);
    styleElement.textContent = styles;
    document.head.append(styleElement);
  };

  const _createStyles = (component) => {
    const styles = component.styles;
    const methods = _getProp("methods", component);
    const state = component.state;
    const ctx = _createSelector(factory.name);
    if (!styles || typeof styles !== "function") return "";

    return styles({ ctx, css, methods, state });
  };

  const _removeChildNodes = (nodeElement) => {
	while (nodeElement.firstChild) {
	  nodeElement.removeChild(nodeElement.lastChild);
	}
  }

  const _getState = (data, component) => {
	if(!data || !data.hasOwnProperty('state')) return component.state.get()
	return data.state
  }

  const _getProps = (data, component) => {
	  if(!data || !data.hasOwnProperty('props')) return getPropsFrom(component.element)
	  return data.props
  }

  const _injectNodes = (data, component) => {
    const methods = _getProp("methods", component);
    const payload = {state: _getState(data, component), props: _getProps(data, component)}
	  const componentTemplate = component.template({  ...payload, html, css, methods });
    const  slotTemplates = _dom.queryAll('[slot-id]', component.element);
	_removeChildNodes(component.element);
    component.element.insertAdjacentHTML('beforeend', componentTemplate);
    _injectSlotTemplate(slotTemplates, component)
    _bindListeners(component)
  };

  const _injectSlotTemplate = (slotTemplates, component) => {
    if(!slotTemplates.length) return

    slotTemplates.forEach( templateElement => {
      const slotId = templateElement.getAttribute('slot-id')
      const slotElement = _dom.queryOnce(`[id=${slotId}]`, component.element)
      slotElement.after(...templateElement.childNodes)
	    slotElement.remove()
    })
  }
  const _render = (component, data) => {
    _execHooks("beforeOnRender", component);
    _bindStyles(_createStyles(component));
    _injectNodes(data, component);
    _bindListeners(component);
    _renderChildren(component);
    _execHooks("afterOnRender", component);
  };

  const _createElements = () => {
    const selector = _createSelector(factory.name);
    return _dom.queryAll(selector, context);
  };

  const _createComponents = (elements) => {
    return elements.map((element) => {
      return factory(element);
    });
  };

  const _execComponents = (components) => {
    components.forEach((component) => {
      _execHooks("beforeOnInit", component);
      let deepState = component.hasOwnProperty("state")
        ? component.state.get()
        : {};
      let deepProps = component.hasOwnProperty("props")
        ? component.props.get()
        : {};

      component.state.on((state) => {
        deepState = state;
        _render(component, { state, props: deepProps });
      });

      if (component.props) {
        component.props.on((props) => {
          deepProps = props;
          _render(component, { state: deepState, props });
        });
      }
      _render(component);
      _execHooks("afterOnInit", component);
    });
  };

  const _execCallback = (callback) => {
    if (!callback) return;
    if (typeof callback !== "function") return;

    callback();
  };

  const init = (callback) => {
    const elements = _createElements();
    const components = _createComponents(elements);
    _execComponents(components);
    _execCallback(callback);
  };

  return { init };
};
