import { createApp } from "./createApp.factory.js";
import { createRouter } from "./createRouter.factory.js";

const lemeJs = ({ appMain, routes = [] }) => {
  const _router = createRouter();

  if (!appMain || typeof appMain !== "function") {
    throw new Error("appMain not is an function and must be.");
  }

  const _initRouter = () => {
    _router.setRoutes(routes);
    _router.init();
  };

  const init = () => {
      const component = createApp(appMain);
      component.init(_initRouter)
  };

  return {
    init,
  };
};

export { lemeJs };
